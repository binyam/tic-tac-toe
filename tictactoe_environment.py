import random
import unittest
"""
Created by Binyam Gebrekidan Gebre @ 2015
"""


class TicTacToeEnvironment:

    def __init__(self):
        """
        start state is represented as (0, 1, 2, 3, 4, 5, 6, 7, 8) - tuple
        """
        self.players = ('X', 'O') 
        self.opponent = {'X':'O', 'O':'X'}
        self.player = random.choice(self.players)
        self.reset()

    def getOpponent(self, player):
        """
        Returns the opponent player
        """
        return self.opponent[player]

    def getCurrentState(self):
        """
        Returns the current state of enviornment
        """
        return self.state

    def getNextToPlay(self):
        """
        Returns the play who is to play next
        """
        return self.player

    def setCurrentState(self, state):
        """
        Sets the current state of enviornment
        """
        self.state = state

    def getPossibleActions(self, state):
        """
        Returns possible actions the agent
        can take in the given state. 
        """
        if self.isWon(state):
            return set([]) 

        initial_state = range(len(self.state))
        return set(initial_state).intersection(set(state))


    def doAction(self, action, player):
        """
        Performs the given action in the current
        environment state and updates the enviornment.

        Returns a reward
        """
        actions = self.getPossibleActions(self.state)
        reward = 0.0
        if action in actions:
            self.state = self.getAfterstate(self.state, action, player)
            reward = self.getReward(self.state,player)
        return self.state, reward

    def getAfterstate(self, state, action, player):
        """
        Returns the state resulting from taking an action
        """
        tempState = list(state)
        tempState[action] = player
        state = tuple(tempState)
        return state

    def switchPlayerTurn(self):
        """
        Switches players turn
        """
        self.player = self.getOpponent(self.player)

    def getReward(self, state, player):
        """
        Returns rewards of 0,- 1 or 1
        """
        if self.hasWon(state,player):
            return 1.0
        elif self.hasWon(state, self.getOpponent(player)):
            return  -1.0
        else:
            return 0.0

    def reset(self):
        """
        Resets the current state to the start state
        Switches player turns
        """
        self.player = self.getOpponent(self.player)
        self.state = tuple(range(9))

    def isGameOver(self, state=None):
        """
        Returns true if the enviornment entered a terminal
        state (when no more actions are left or when either player has won)
        """
        if not state:
          state = self.getCurrentState()

        actions = self.getPossibleActions(state)
        return len(actions) == 0 or self.isWon(state)

    def isWon(self, state):
        """
        Returns true if either player has won
        """
        return self.hasWon(state,self.players[0]) or self.hasWon(state, self.players[1])

    def hasWon(self, state, player):
        """
        Returns True if either player ('X' or 'O') wins
        """
        return set(state[0:3]) ==  {player} or \
            set(state[3:6]) ==  {player} or \
            set(state[6:9]) ==  {player} or  \
            set(state[0::3]) == {player} or \
            set(state[1::3]) == {player} or \
            set(state[2::3]) == {player} or \
            set(state[::4]) == {player} or \
            set(state[2:7:2]) == {player} 

class TicTacToeEnvironmentTestCase(unittest.TestCase):
    def setUp(self):
        self.te = TicTacToeEnvironment()

    def testGetCurrentState(self):
        self.assertEqual(self.te.getCurrentState(), tuple(range(9)))

    def testDoAction(self):
        self.te.reset()
        action = 2
        player = 'X'
        nextState, reward = self.te.doAction(action, player)
        self.assertEqual(nextState, (0, 1, 'X', 3, 4, 5, 6, 7, 8))

    def testGetAfterstate(self):
        state = ('O', 'O', 2, 'X', 4, 5, 'X', 7, 8)
        player = 'X'
        action = 8
        afterstate = self.te.getAfterstate(state, action, 'X')
        self.assertEqual(afterstate, ('O', 'O', 2, 'X', 4, 5, 'X', 7, 'X'))
    
    def testGetPossibleActions(self):
        state = ('X', 1, 2, 3, 4, 5, 6, 7, 8)
        possibleActions = {1, 2, 3, 4, 5, 6, 7, 8}
        self.assertEqual(self.te.getPossibleActions(state),possibleActions)

        state = ('X', 'O', 'X', 'O', 'X', 'X', 'X', 'O', 'O')
        possibleActions = set([])
        self.assertEqual(self.te.getPossibleActions(state),possibleActions)

    def testGetReward(self):
        state = ('O', 'O', 'O', 'X', 4, 5, 'X', 7, 8)
        reward = self.te.getReward(state,'X')
        self.assertEqual(reward,0)

        reward = self.te.getReward(state,'O')
        self.assertEqual(reward,1)

        state = ('O', 'O', 'X', 'X', 'X', 'O', 'O', 'O', 8)
        reward = self.te.getReward(state,'X')
        self.assertEqual(reward,0)

    def testGetNextToPlay(self):
        self.te.reset()
        player = self.te.getNextToPlay()
        self.te.switchPlayerTurn()
        nextPlayer = self.te.getNextToPlay()
        self.assertEqual(nextPlayer, self.te.getOpponent(player))

    def testGetOpponent(self):
        self.assertEqual('O', self.te.getOpponent('X'))

    def testReset(self):
        state = ('O', 'O', 'O', 'X', 4, 5, 'X', 7, 8)
        self.te.setCurrentState(state)
        self.te.reset()
        state = self.te.getCurrentState()
        self.assertEqual(state, tuple(range(9)))

    def testIsGameOver(self):
        finishStates = {'horizontals': [('O', 'O', 'O', 3, 4, 5, 6, 7, 8),
                                        (0, 1, 2,'O', 'O', 'O', 6, 7, 8),
                                        (0, 1, 2, 3, 4, 5, 'O', 'O', 'O')],
                        'verticals':  [('O', 1, 2, 'O', 4, 5, 'O', 7, 8),
                                      (0, 'O', 2, 3, 'O', 5, 6, 'O', 8),
                                      (0, 1, 'O', 3, 4, 'O', 6, 7, 'O')],
                        'diagonals': [('O', 1, 2, 3, 'O', 5, 6, 7, 'O'),
                                      (0, 1, 'O', 3, 'O', 5, 'O', 7, 8)],
                        'noActionLeft': [('X', 'X', 'O', 'O', 'O', 'X',' X', 'O', 'O')]}

        for key in finishStates:
            for state in finishStates[key]:
                self.assertEqual(self.te.isGameOver(state),True)
        

if __name__ == '__main__':
    unittest.main()
