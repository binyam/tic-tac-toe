from tictactoe_environment import TicTacToeEnvironment 
import collections
import random
import copy
import matplotlib.pyplot as plt

"""
Created by Binyam Gebrekidan Gebre @ 2015
"""

class RLAgent:

    def __init__(self, player='X', learningRate=0.5, epsilon=0.3, discount=1.0, learningAlgorithm="qLearn", numEpisodes=30000):
        """
        alpha    - learning rate
        epsilon  - exploration rate
        gamma    - discount factor
        numEpisodes - number of training episodes
        """
        self.player = player
        self.env = TicTacToeEnvironment()
        self.alpha = float(learningRate)
        self.epsilon = float(epsilon)
        self.discount = float(discount)
        self.numEpisodes = int(numEpisodes)

        self.episodesSoFar = 0
        self.accumTrainRewards = 0.0
        self.rewards = []
        self.learningAlgorithm = learningAlgorithm
        # a dictionary of dictionary (states and then actions)
        self.Q = collections.defaultdict(lambda : collections.defaultdict(lambda :0.0))

    def train(self):
        print "Learning..."
        for i in range(self.numEpisodes):
            self.runEpisode()

    def startEpisode(self):
        """
        Resets the environment to initial state
        """
        self.env.reset()
        self.episodeRewards = 0.0

    def runEpisode(self):
        """
        Runs an episode
        """
        if self.learningAlgorithm == "qLearn":
            self.qLearn()
        else:
            self.sarsaLearn()

    def stopEpisode(self):
        """
        Computes rewards per episode and 
        Also, it stops and exploration after training episodes have finished running
        """
        self.episodesSoFar += 1
        if self.episodesSoFar < self.numEpisodes:
            self.accumTrainRewards += self.episodeRewards
            avg_reward = self.accumTrainRewards/float(self.episodesSoFar)
            self.rewards.append(avg_reward)
            print "Episode = {0}, average reward per episode = {1}".format(self.episodesSoFar, avg_reward)

        if self.episodesSoFar >= self.numEpisodes:
            # no exploration (epsilon = 0.0 ) and no learning (alpha = 0.0)
            print "Agent finished learning from {0} episodes".format(self.numEpisodes)
            print "="*80
            print 
            self.epsilon = 0.0    
            self.alpha = 0.0      

    def qLearn(self):
        """
        Trains agent using qLearning algorithm
        """
        self.startEpisode()
        while not self.env.isGameOver():
            player = self.env.getNextToPlay()
            state = self.env.getCurrentState()
            action = self.selectAction(state, player)

            nextState, reward = self.env.doAction(action, player)
            self.episodeRewards += reward

            opponent = self.env.getOpponent(player)

            # updates states (backup)
            target = reward - self.discount * self.getMaxQValue(nextState, opponent)
            change = target - self.getQValue(state, player, action)
            self.Q[(state,player)][action] = self.getQValue(state, player, action) + self.alpha * change

            # switch player turns
            self.env.switchPlayerTurn()

        self.stopEpisode()
  
    def sarsaLearn(self):
        """
        Trains agent using sarsa learning algorithm
        """
        self.startEpisode()
        
        player = self.env.getNextToPlay()
        state = self.env.getCurrentState()
        action = self.selectAction(state, player)
        while not self.env.isGameOver():
            player = self.env.getNextToPlay()
            state = self.env.getCurrentState()

            nextState, reward = self.env.doAction(action, player)
            self.episodeRewards += reward

            opponent = self.env.getOpponent(player)
            nextAction = self.selectAction(nextState, opponent) 
            
            # updates states (backup)
            target = reward - self.discount * self.getQValue(nextState, opponent, nextAction)
            change = target - self.getQValue(state, player, action)
            self.Q[(state,player)][action] = self.getQValue(state, player, action) + self.alpha * change

            action = nextAction
            self.env.switchPlayerTurn()

        self.stopEpisode()  

    def selectAction(self, state, player):
        """
        Selects the best action most of the time.  With
        probability self.epsilon, it takes a random action
        """
        possibleActions = self.env.getPossibleActions(state)
        
        if not possibleActions:
            return None

        explore = random.random() < self.epsilon     
        if explore:
            action = random.choice(list(possibleActions))
        else:
            action = self.getPolicy(state,player)
        return action

    def getPolicy(self, state, player):
        """
        Returns the best action to take for a given state and player. 
        """
        actions = self.env.getPossibleActions(state)

        QValues = [(self.getQValue(state, player, action), action) for action in actions]
        
        if not QValues:
            return None
        return max(QValues,key=lambda x:x[0])[1]

    def getQValue(self, state, player, action):
        """
        Returns Q-action value
        """
        if self.env.isWon(state):
            return 0.0
        randValue = random.uniform(-0.15,0.15) 
        return self.Q[(state, player)].get(action,randValue)  

    def getMinQValue(self, state, player):
        """
        Returns the value of the action that results in the lowest Q-value
        """
        if self.env.isWon(state):
            return 0.0

        actions = self.env.getPossibleActions(state)
        Qvalues = [(self.getQValue(state, player, action), action) for action in actions]
                
        if Qvalues:
            return min(Qvalues)[0]
        else:
            return 0.0  

    def getMaxQValue(self, state, player):
        """
        Returns the value of the action that results in the highest Q-value
        """
        if self.env.isWon(state):
            return 0.0

        actions = self.env.getPossibleActions(state)
        Qvalues = [(self.getQValue(state, player, action), action) for action in actions]
        if Qvalues:
            return max(Qvalues) [0]
        else:
            return 0.0  

    def plotAverageReward(self):
        """
        Plots average reward per episode
        """
        if self.rewards:
            plt.plot(self.rewards)
            plt.ylim(0, 1.1)
            plt.xlabel('Training episodes')
            plt.ylabel('Reward per episode')
            plt.savefig('avg_reward_per_episode_{0}.png'.format(self.learningAlgorithm))
            plt.show()

    def displayQvalues(self, state, player):
        """
        Displays Q-values of a state for a given player
        """

        actions = self.env.getPossibleActions(state)
        Qvalues = [(self.getQValue(state, player, action), action) for action in actions]

        stateQvalues = list(state)
        for value, action in Qvalues:
            stateQvalues[action] = value

        stateQvalues = tuple(stateQvalues)
        display(stateQvalues)



def give_instructions():
    """
    Displays instructions
    """
    print(
    """
    Welcome to the game of Tic-Tac-Toe!

    This will be a showdown between you and an agent.  

    You will make your move known by entering a number, 0 - 8.  The number 
    will correspond to the board position as illustrated:

                    0 | 1 | 2
                    ---------
                    3 | 4 | 5
                    ---------
                    6 | 7 | 8

    The purpose of the game is to line up three symbols in a row (vertically, horizontally or diagonally)
    """
    )

def display(state):
    template = """
    0a | 1b | 2c
    ---------
    3d | 4e | 5f
    ---------
    6g | 7h | 8i
    """
    placeHolders = ['0a', '1b', '2c', '3d', '4e', '5f', '6g', '7h', '8i']
    for i, sym in enumerate(placeHolders):
        val = state[i]
        if isinstance(val,float):
            val = round(val,3)
        template = template.replace(sym, str(val))
    print template


def main():
    give_instructions()

    agent = RLAgent()
    agent.train()


if __name__== '__main__':
    main()
