import random
from rlagent import RLAgent
import copy
import collections
import argparse
import cPickle as pickle
"""
Created by Binyam Gebrekidan Gebre @ 2015
"""

def give_instructions():
    """
    Displays instructions
    """
    print(
    """
    Welcome to the game of Tic-Tac-Toe!

    The purpose of the game is to line up three symbols in a row (vertically, horizontally or diagonally). 

    You will make your move known by entering a number, 0 - 8.  The number 
    will correspond to the board position as illustrated:

                    0 | 1 | 2
                    ---------
                    3 | 4 | 5
                    ---------
                    6 | 7 | 8

    """
    )

 	
def getHumanMove(state):
	"""
	Takes input from the user (Expects one of 0,1,2...,8)
	"""
	display(state)
	move = input('Enter your move (0-8): ')
	legalActions = getLegalActions(state)
	return move if move in legalActions else getHumanMove(state)

def display(state):
    template = """
    0a | 1b | 2c
    ---------
    3d | 4e | 5f
    ---------
    6g | 7h | 8i
    """
    placeHolders = ['0a', '1b', '2c', '3d', '4e', '5f', '6g', '7h', '8i']
    for i, sym in enumerate(placeHolders):
        val = state[i]
        if isinstance(val,float):
            val = round(val,3)
        template = template.replace(sym, str(val))
    print template

def getLegalActions(state):
	"""
	Return the possible actions that can be taken in a given state
	"""
	return set(range(len(state))).intersection(set(state))

def wins(state, player):
	"""
	Returns true if player wins 
	"""
	return set(state[0:3]) ==  {player} or \
		set(state[3:6]) ==  {player} or \
		set(state[6:9]) ==  {player} or  \
		set(state[0::3]) == {player} or \
		set(state[1::3]) == {player} or \
		set(state[2::3]) == {player} or \
		set(state[::4]) == {player} or \
		set(state[2:7:2]) == {player} 

def isGameOver(state):
	"""
	Returns true if either player wins or no action is left
	"""
	actions = getLegalActions(state)
	return len(actions) == 0 or wins(state,"X") or wins(state, "O")

def getRandomAction(state):
	"""
	Returns random action from the possible actions in the given state
	"""
	actions = getLegalActions(state)
	return random.choice(list(actions))

def takeAction(state, action, player):
	"""
	Performs action and returns the next state
	"""
	tempState = list(state)
	tempState[action] = player
	state = tuple(tempState)
	return state

def announceResult(state):
	"""
	Displays results
	"""
	if wins(state,"X"):
		print "X won!"
	elif wins(state,"O"):
		print "O won!"
	else:
		print "Tie!"

def successors(state,player):
	"""
	Returns a dictionary of next possible states in the form of {state:action} pairs 
	"""
	actions = getLegalActions(state)
	state_action = {}
	for action in actions:
		nextState = copy.deepcopy(list(state))
		nextState[action] = player
		state_action[tuple(nextState)] = action

	return state_action

def minimax_player(state, player):
	"""
	Returns the best move according to minimax algorithm
	"""
	return minimax(state,player)[1]

#@decorator
def memo(f):
	"""
	Decorator that caches the return value for each call to f(args).
	"""
	cache = {}
	def _f(*args):
		try:
			return cache[args]
		except KeyError:
			cache[args] = result = f(*args)
			return result
		except TypeError:
			return f(*args)
	return _f

@memo
def minimax(state, player):
	"""
	Returns the best score and best move for a given state and player
	"""
	opponent = {'O': 'X', 'X': 'O'}[player]
	if isGameOver(state):
		if wins(state, player): return 1, None
		elif wins(state,opponent): return -1,None
		else: return 0,None

	bestScore = -2
	bestMove = None
	for (nextState, action) in successors(state, player).items():
		score = -minimax(nextState, opponent)[0]
		if score > bestScore:
			bestScore, bestMove = score, action
	return bestScore, bestMove

def whoWon(state):
	"""
	Returns the winner or tie
	"""
	if  wins(state,"X"):
		return "X"
	elif  wins(state, "O"):
		return "O"
	else:
		return "Tie"

def parseInput():
	"""
	Parses user command input
	"""
	parser = argparse.ArgumentParser()
	
	parser.add_argument('-p', '--player',action='store', choices=["X", "O"], default='X', 
						help='Agent name: (default: %(default)s)')

	parser.add_argument('-d', '--discount',action='store', type=float,default=1.0, 
						help='Discount on future reward (default: %(default)s)')

	parser.add_argument('-e', '--epsilon',action='store',type=float,default=0.3,
	                 	help='Chance of taking a random action in q-learning (default: %(default)s)')

	parser.add_argument('-r', '--learningRate',action='store', type=float,default=0.5,
	                 	help='learning rate (default: %(default)s)')

	parser.add_argument('-n', '--numEpisodes',action='store',type=int,default=30000,
	                	help='Number of epsiodes used to run agent (default: %(default)s)')

	parser.add_argument('-o', '--opponent',action='store',choices=["random", "minimax", "human"],default="random",
	                	help='The opponent to play against (default: %(default)s)')

	parser.add_argument('-g', '--numGames',action='store',type=int,default=1000,
	                	help='Number of games to play (default: %(default)s)')

	parser.add_argument('-l', '--learningAlgorithm',action='store',choices=["qLearn", "sarsaLearn"],default="qLearn",
	                 	help='Learning types (options are qLearn and sarsaLearn and default: %(default)s)')

	parser.add_argument('-s', '--showRewardGraph',action='store_true' ,default=False,
						help="Show average reward per episode")

	args = parser.parse_args()
	return args

def main():
	args = parseInput()
	
	trainOpts = {'player': args.player,
				'discount': args.discount,
				'learningRate': args.learningRate,
				'epsilon': args.epsilon,
				'learningAlgorithm': args.learningAlgorithm,
				'numEpisodes': args.numEpisodes}

	agent = RLAgent(**trainOpts)
	agent.train()
	if args.showRewardGraph:
		agent.plotAverageReward()

	winnerTable = collections.defaultdict(lambda :0)
	numGames = args.numGames

	player = args.player
	opponent = {'O': 'X', 'X': 'O'}[player]
	
	# opponentAgent = getRandomAction
	# if args.opponent == 'human':
	# 	opponentAgent = getHumanMove

	for i in range(numGames):
		state = tuple(range(9))
		#display(state)
		agentTurn = random.choice(['X','O']) == player
		while not isGameOver(state):
			action = None
			whoIsNext = None
			if agentTurn: 
				action = agent.getPolicy(state,player)
				whoIsNext = player
			else:
				if args.opponent == 'human':
					action = getHumanMove(state)
				elif args.opponent == 'minimax':
					action = minimax_player(state,opponent)
				else:
					action = getRandomAction(state)
				whoIsNext = opponent

			state = takeAction(state,action,whoIsNext)
			#display(state)
			agentTurn = not agentTurn
		
		winner =  whoWon(state) 
		print("Game = {0}, winner = {1}".format(i, winner))
		winnerTable[winner] += 1
	
	print 
	print "Total number of games: ", numGames
	print "Scores: "
	for key, humanReadable in {'X':'X wins (%)', 'O':'O wins (%)', 'Tie':'Ties (%)'}.items():
		print "-", humanReadable, "=", winnerTable[key] * 100 /float(numGames)

if __name__== '__main__':
	main()
