TIC-TAC-TOE
===========

The files in this repository contain code to train and play TIC-TAC-TOE.

The start program is called  ```ttt-play.py``` and allows us to train an agent with different parameters.

For example
-----------
1. Train with 40,000 episodes and play 1,000 games with random agent and show scores.

    ```python ttt_play.py -n 40000 -o random -g 1000``` 

2. Train with 40,000 episodes and play 1,000 games with a minimax agent (the minimax agent never loses).
    
    ```python ttt_play.py -n 40000 -o minimax -g 1000```

3. Generate the graphs in this repository. The graphs show average reward per episode.

    
    ```python ttt_play.py —s -l qLearn -n 200``` or ```python ttt_play.py —s -l sarsaLearn -n 200```

4. More options (exploration, learning rate, discount or also for playing with humans)
    
    ```python ttt_play.py -h```